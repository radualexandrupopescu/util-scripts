:: Name:     ncrack-miseryender.bat
:: Purpose:  Kills ncrack if it starts printing "nsock READ timeout!"
:: Author:   radualexandrupopescu@gmail.com
:: Revision: 17 May 2017 - original implementation.

@ECHO OFF
SETLOCAL ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION

:: *************************************
:: ** MAIN VARIABLES                  **
:: ** ------------------------------- **
:: ** These controll script behaviour **
:: *************************************

:: tee output file name
:: This file will be parsed for indicators of ncrack hanging
:: !! WARNING !!
:: THIS should be THE SAME VALUE as in ncrack-autorunner.bat
SET teeoutputfilename=ncrack.tee.txt

:: Kill threshold
:: Defines the maximum number of lines to be encountered
:: before we kill the process
SET /A killthresh=32

:: Enable Interval mode: 1=enable, 0=disable
:: This flag will make the script clear the output of the
:: tee file after each run, effectively making it check
:: on an per-interval basis.
SET /A intervalmode=1

:: Error exit codes
SET /A ERROR_UNKNOWN=-1
SET /A ERROR_OK=0
SET /A ERROR_NO_BOOKMARK_FOUND=1
SET /A ERROR_NO_MORE_DICTS=2
SET /A ERROR_FINDSTR_UNKNOWN_EXIT_CODE=3

:: variables
SET me=%~n0
SET /A errno=%ERROR_OK%
SET /A passwddictcnt=0


:: *************************************
:: ** MAIN PROGROM LOGIC              **
:: *************************************

:MAINLOGIC
:: Define the cmd that checks for errors
SET "cmd=findstr /R /N "nsock READ timeout" %teeoutputfilename% | find /C ":""
:: Count instances
FOR /F %%a IN ('!cmd!') DO SET number=%%a
:: Kill the process
IF /I "%number%" GTR "%killthresh%" (
    TASKKILL /IM "nckrack*" /F
)

IF intervalmode (
    type nul>%teeoutputfilename%
)

:END
ENDLOCAL
ECHO ON
@EXIT /B %errno%