# Scripts description

The following scripts are developed by me (except where stated otherwise) for different sittuations/reasons.

## ncrack-autorunner.bat

Configures `ncrack` to run with different dictionaries and autoresume.
Meant to be used both as launcher and startup script.

## ncrack-miseryender.bat

Kills ncrack if it starts printing `nsock READ timeout`.
It's meant to be used in conjuction with **ncrack-autorunner.bat** as a scheduled task.

## ncrack-peruserpass.bat

Confugures `ncrack` to run in parallel with different users each with it's own password dict and kills all the processes when one of them finds a valid login.

## tee.bat

*NIX `tee` command simulator in Batch script.
Copied from <http://www.robvanderwoude.com/unixports.php#TEE>
