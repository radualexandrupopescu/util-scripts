:: Name:     ncrack-autorunner.bat
:: Purpose:  Configures ncrack to run with different dictionaries and autoresume
::           Meant to be used both as launcher and startup script.
::           Read below to see how to configure
:: Author:   radualexandrupopescu@gmail.com
:: Revision: 24 May 2016 - initial version based on a Proof Of Concept
::           16 May 2017 - implementation of output piping for killing when hung.

@ECHO OFF
SETLOCAL ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION



:: *************************************
:: ** MAIN VARIABLES                  **
:: ** ------------------------------- **
:: ** These controll script behaviour **
:: *************************************

:: Enable backwards compatibility: 1=enable, 0=disable
:: There seems to be a bug in ncrack 0.5 and the only other version I had access
:: to was 0.4ALPHA witch does not have the --save flag.
:: This option makes the script not add the resume flag. This disables resumming
:: for now because some logic to enable resumming with the default file names
:: has yet to be implemented. It will only allow for a pseudo-resuming, based
:: on the bookmark file.
:: If you want to manually force it, uncomment the following line
:: SET /A compatmode=1
IF NOT DEFINED compatmode (
	:: Change the ip if you belive it's real
	ncrack http://127.255.255.254,CL=1,at=1,cd=10ms,to=1ms -vv -d10 --save kkk.txt >NUL 2>&1
	IF /I "%ERRORLEVEL%" EQU "%ERROR_OK%" (
		SET /A compatmode=0
		DEL kkk.txt
	) ELSE (
		SET /A compatmode=1
	)
)

:: Target definition
:: This is the "{target and service specification}" from the ncrack usage help.
:: As far as I know you should be able to have multiple targets in any format
:: supported by ncrack (full spec, host, host+port), but I can not guarantee
:: this script's logic will work for multiple targets.
:: See https://nmap.org/ncrack/man.html for more info.
SET ncracktargets=rdp://localhost:3389,CL=1,cd=100ms

:: Set output file name
SET ncrackoutfile=ncrack.out.txt

:: Controll stopping after first valid credentials
:: (1=enable,0=disable; set's the -f ncrack flag)
:: I would recommend setting this to 1 because as I stated above, I can not
:: guarantee this script's logic will work otherwise.
SET /A stopatfirstmatch=1

:: Controll verbosity level (equivalent to ncrack -v/-vv/no v)
:: Numeric value used to set verbosity:
::   0: no -v flag added
::   1: -v flag added
::   2: -vv flag added
SET /A verbositylevel=2

:: Controll debug level (equivalent to ncrack -d#)
SET /A debuglevel=10

:: Dictionary file base name
:: Can be used to set the path file path to the dictionaries also.
:: Ex.: C:\dicts\dict
SET passwddictbasename=pass

:: Dictionary file extension (please do not leave empty, logic does not allow)
SET passwddictext=txt

:: The way the above variables work is in a loop, a variable named
:: "passwddictcnt" will increment itself
:: (thus cycling through values 1, 2, 3, etc) and the dictionary file name will
:: be composed by concatenating the "passwddictbasename" variable, with the
:: "passwddictcnt" variable and the "passwddictext" variable
:: (preceded by a "." - dot)
:: Example ("passwddictbasename"="dict", "passwddictext"="txt"):
::   "passwddictcnt"="1" => "passwddict"="dict1.txt"
::   "passwddictcnt"="2" => "passwddict"="dict2.txt"
::   "passwddictcnt"="3" => "passwddict"="dict3.txt"

:: Controll username
:: If the value matches a file, then the ncrack -U flag will be used (meaning
:: ncrack will load usernames from a file) else will be interpreted as a list
:: of usernames, comma-separated as expected by --user flag
SET userlist=Administrator

:: ncrack resume file name
:: Can be used to set the file path also.
:: Ex.: C:\ncrack-workdir\resume.txt
SET ncrackresumefilename=ncrack.resume.txt

:: Bookmark file name
:: This is used in case ncrack is interrupted while running to determine the
:: next dictionary to use. Also used to prevent the running of the script in
:: case the script is used as startup script, the script has finished running,
:: and you have not checked the results. If it would run from begining, it would
:: clobber the output. Hence if it is found it will change the value for used
:: as the next dictionary, witch in case the script has finished will not exist
:: and the script will just exit
SET bookmarkfilename=ncrack.bookmark.txt

:: ncrack executable
:: Can be used to set full path to executable if not on "PATH"
SET ncrack=ncrack

:: Enable tee (write output to screen and file): 1=enable, 0=disable
:: This is used to check for an edge case that occurs (as far as I know)
:: when ncrack is scanning a RDP server and the server is stopped/resetted;
:: in this case ncrack kind of hangs.
:: By using a different script to parse this file and kill ncrack if it
:: encouters the error messages we can avoid the issue
SET teeenabled=1

:: tee output file name
:: This file will be parsed for indicators of ncrack hanging
SET teeoutputfilename=ncrack.tee.txt

:: tee executable
:: Can be used to set full path to executable if not on "PATH"
SET tee=tee.bat


:: Error exit codes
SET /A ERROR_UNKNOWN=-1
SET /A ERROR_OK=0
SET /A ERROR_NO_BOOKMARK_FOUND=1
SET /A ERROR_NO_MORE_DICTS=2
SET /A ERROR_FINDSTR_UNKNOWN_EXIT_CODE=3

:: variables
SET me=%~n0
SET /A errno=%ERROR_OK%
SET /A passwddictcnt=0


:: *************************************
:: ** MAIN PROGROM LOGIC              **
:: *************************************

:: Check if we already found any credentials. If we did, do not start.
IF EXIST "%ncrackoutfile%" (
	FIND "Discovered credentials for" %ncrackoutfile% >NUL 2>&1 && (
		ECHO Found credentials.
		GOTO END
	)
)

:: Check to see if there is a resume file
:: TODO: fix resuming when using compatibility mode
IF EXIST "%ncrackresumefilename%" (
	:: Announce the discovery and commence resume
	ECHO Resumming due to discovery of %ncrackresumefilename%
	%ncrack% --resume "%ncrackresumefilename%"
	
	:: Discard resume file now that we finished the task
	DEL /F "%ncrackresumefilename%"
	
	:: Check if we have a bookmark file
	IF EXIST "%bookmarkfilename%" (
		:: We found it so load it's value into passwddictcnt
		FOR /F %%i in ('type "%bookmarkfilename%'") DO (
			SET /A passwddictcnt=%%i
		)
	) ELSE (
		:: Well this is really strange.
		:: We found a resume file but no bookmark file.
		:: This should never have happened.
		:: !!! FULL ON PANIC !!!
		ECHO Encountered suspicious circumstances:
		ECHO   found a resume file and no bookmark
		ECHO ### ERROR ###
		SET /A errno=%ERROR_NO_BOOKMARK_FOUND%
		GOTO END
	)
)
:: Compatibility mode resume
IF /I "%compatmode%" EQU "1" (
	IF EXIST "%bookmarkfilename%" (
		:: We found it so load it's value into passwddictcnt
		FOR /F %%i in ('type "%bookmarkfilename%"') DO (
			SET /A passwddictcnt=%%i
		)
		:: We tell it to rerun with the dictionary from bookmark as a way to
		:: resume.
		SET /A passwddictcnt=!passwddictcnt!-1
	)
)


:: Main program loop
:NCRACKMAINLOOP

:: (Re)Set composed cmd
SET composedcmd=%ncrack%

:: Increment the dictionary file number
SET /A passwddictcnt=%passwddictcnt%+1

:: Compose the dictionary file name for this loop run
SET passwddict=%passwddictbasename%%passwddictcnt%.%passwddictext%

:: Announce the dictionary used
ECHO Attempting to use "%passwddict%"

:: Check if the file exists
IF NOT EXIST "%passwddict%" (
	ECHO File "%passwddict%" was not found. Assuming we ran out of dictionaries.
	ECHO We did not find any credentials.
	SET /A errno=%ERROR_NO_MORE_DICTS%
	GOTO END
)

:: Compose the command line
:: Check if we want verbosity
IF /I "%verbositylevel%" EQU "1" (
	SET composedcmd=%composedcmd% -v
) ELSE IF "%verbositylevel%" GEQ "2" (
	SET composedcmd=%composedcmd% -vv
)
:: Check if we want debug (well not really check since -d0 is the same as no -d)
SET composedcmd=%composedcmd% -d%debuglevel%
:: Check if we want to stop on first match
IF /I "%stopatfirstmatch%" EQU "1" (
	SET composedcmd=%composedcmd% -f
)
:: Set resume file name if compatmode is not set
IF /I "%compatmode%" EQU "0" (
	SET composedcmd=%composedcmd% --save "%ncrackresumefilename%"
)
:: Set output file
SET composedcmd=%composedcmd% -oN "%ncrackoutfile%"
:: Check if the username parameter is a list or a file and use accordingly
IF EXIST "%userlist%" (
	SET composedcmd=%composedcmd% -U "%userlist%"
) ELSE (
	SET composedcmd=%composedcmd% --user %userlist%
)
:: Set password dictionary file
SET composedcmd=%composedcmd% -P "%passwddict%"
:: Set the target
SET composedcmd=%composedcmd% %ncracktargets%
:: Check if we tee the output
IF /I "%teeenabled%" EQU "1" (
	SET composedcmd=%composedcmd% ^| %tee% %teeoutputfilename%
)

:: Set the bookmark
ECHO %passwddictcnt% > "%bookmarkfilename%"

:: Call command
%composedcmd%

:: Check if we ran ok
IF /I "%ERRORLEVEL%" EQU "%ERROR_OK%" (
	:: Check if we found any credentials
	FIND "Discovered credentials for" "%ncrackoutfile%" >NUL 2>&1 && (
		ECHO Found credentials.
		GOTO END
	) || (
		:: Go to begining of loop
		GOTO NCRACKMAINLOOP
	)
) ELSE (
	:: ncrack did not run ok. No ideea why. The multitude of checks should have
	:: prevented this.
	ECHO %ncrack% exited with non-zero exit code. No ideea why. Contact dev.
	SET /A errno=%ERROR_UNKNOWN%
	GOTO END
)


:END
ENDLOCAL
ECHO ON
@EXIT /B %errno%